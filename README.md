**Test de API Apollo en iOs para conexión con GitHub**

El presente repositorio es parte de mis pruebas de conexión de las APIS Client Apollo para iOS (Swift 5) y Github ​GraphQL Api


---

## Desarrollo

El desarrollo de esta prueba fue considerado en 3 partes

1. Pruebas de la Api de Apollo Server para entender la logica de su funcionamiento
2. Desarrollo de un proyecto de Xcode (app Swift), con un entorno basico para dar conxión posterior con las Apis externas
3. Instalación del Api de Apollo al proyecto de Xcode.
4. 

---

## Requisitos

1. Xcode y MacOs Catalina
2. Instalación de la Api de Apollo

---

## Desarrollo

La 2da Etapa "Desarrollo" ha de ser descrita en este segmento.

1. Para poder desarrollar el app se escogio Swift 5 con Xcode, ya que esta plataforma tiene documentación extensa para realizar el desarrollo y el tener experiencia previa en el desarrollo de apps en este entorno, en ese entonces Swift 2->3 (trabaje durante el periodo de release/transición de Swift 3)
2. Se desidio usar un desarrollo netamente en codigo, dejando de lado el uso de los storyboard ya que el uso de estos elementos podria concurrir en la omisión de algunos pasos del desarrollo


## Observaciones

Hubieron algunos problemas en las primeras pruebas con Xcode dada mi falta de familiarización con los cambios que se llevaron a cabo desde la versión 3, teniendo como principal cambio el uso de SceneDelegate.



## Instrucciones de instalación de Cliente de Apollo en iOs

Para instalar el Api en nuestra aplicación se utilizara la instalación por medio de "Swift Package", existen otras opciónes sin embargo por un tema de tiempo me limitare a la que he usado.

1. Con nuestro proyecto abierto ir al menú "File->Swift Packages->Add Package Dependency ..."
2. en la ventana que se abrira, copiar : https://github.com/apollographql/apollo-ios.git
3. En caso se requiera una versión previa del repositorio es posible escogerla en al dar "next", en este caso se escogera la que se tiene por defecto y se da "next" nuevamente.
4.  Aparecear una ventana donde iniciara la carga de packetes tras lo cual nos dara opción de instalar las librerias deseados, para nuestro ejemplo se escogera solo Apollo y damos "Finish"

Tras esto aparecera un nuevo segmento en la barra de navegación de Xcode ( a la izquierda del area de trabajo), en ellas se enlistaran las nuevas librerias disponibles.

Instalar "Apollo-codeGen", via terminal ejecutar :
```
    $ npm install -g apollo-codegen
```
En el proyecto, seleccionar el icono del projecto, Target "AleTest" y seleccionar Build Phases, Agregar una nueva " y colocarla (drag & drop)  antes que Compile Sources 

## Agregar un esquema al proyecto

Para agregar el esquema de Github al proyecto, es necesario obtener un token, para ello debemos:

1. ir a : https://github.com/
2. Logear con nuestro respectivo usuario
3. ir a : https://github.com/settings/tokens/new
4. Colocar alguna Nota para recordar a que pertenece este Token, para nuestro ejemplo "GraphQL"
5. Seleccionar las opciones que requiramos para el acceso, en nuestro caso : repo, admin:org y user
6. Dar al Click boton : Generate Token (al final del formulario) 
7. IMPORTANTE: Copiar y guardar el codigo Token generado

8. En caso no tener instalado Apollo, ejecutar en la terminal : npm install -g apollo
9. En la terminal nos nos dirigimos a la ruta del projecto. Creamos un subdirectorio "schema" y ya en el ejecutamos el comando:
```apollo schema:download --endpoint=http://api.github.com/graphql schema.json --header="Authorization: Bearer you_generated_token"
```
Remplazar your_generated_token, por el codigo generado en el paso 6

10. Desde Xcode, damos click derecho a la carpeta schema en nuestro navegador (espacio a la izquierda del editor) y agregamos el archivo generado en el paso anterior

11. Agregar una nueva fase de compilación "Build Phase"
    - En el navegador seleccionar el proyecto AleTest.
    - En la pestaña horizontal de TARGETS, seleccionar AleTest
    - Seleccionar Build Phases
    - Agregar una nueva fase (Boton [+]), nombrarla Apollo GraphQl
    - Colocarla la nueva fase antes  de "Compile Sources"
    - Pegar en el area del scrip de Shell:
    
    ```
    while ! [ -d "${DERIVED_DATA_CANDIDATE}/SourcePackages" ]; do
      if [ "${DERIVED_DATA_CANDIDATE}" = / ]; then
        echo >&2 "error: Unable to locate SourcePackages directory from BUILD_ROOT: '${BUILD_ROOT}'"
        exit 1
      fi

      DERIVED_DATA_CANDIDATE="$(dirname "${DERIVED_DATA_CANDIDATE}")"
    done

    # Grab a reference to the directory where scripts are checked out
    SCRIPT_PATH="${DERIVED_DATA_CANDIDATE}/SourcePackages/checkouts/apollo-ios/scripts"

    if [ -z "${SCRIPT_PATH}" ]; then
        echo >&2 "error: Couldn't find the CLI script in your checked out SPM packages; make sure to add the framework to your project."
        exit 1
    fi
 
    cd "${SRCROOT}/${TARGET_NAME}"
    "${SCRIPT_PATH}"/run-bundled-codegen.sh codegen:generate --target=swift --includes=./**/*.graphql --localSchemaFile="schema.json" API.swift ```
 ## Token
 
 Colocar el token que generen con sus cuentas GitHub en el aarchivo: ViewController.swift. en la linea 12, remplazando la asignación que alli se tiene
 let git_hub_token = "<pegar token aqui>"
