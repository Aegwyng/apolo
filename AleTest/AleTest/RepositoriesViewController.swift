//
//  RepositoriesViewController.swift
//  AleTest
//
//  Created by Alejandra Camargo on 11/3/19.
//  Copyright © 2019 Alejandra Camargo. All rights reserved.
//

import UIKit

class RepositoriesViewController: UIViewController{
    
    override func loadView() {
        super.loadView()
        self.view.backgroundColor = .systemOrange
    }
    
}
