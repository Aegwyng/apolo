//
//  ViewController.swift
//  AleTest
//
//  Created by Alejandra Camargo on 11/3/19.
//  Copyright © 2019 Alejandra Camargo. All rights reserved.
//

import UIKit
import Apollo

let git_hub_token = "<pegar token aqui>"


class ViewController: UIViewController{
    
    let title_label : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Github Repositories"
        label.font = UIFont.init(name: "Helvetica Neue", size: CGFloat(integerLiteral: 35))
        return label
    }()
    
    lazy var search_bar : UISearchBar = {
        let search_bar = UISearchBar()
        search_bar.translatesAutoresizingMaskIntoConstraints = false
        search_bar.sizeToFit()
        search_bar.isTranslucent = false
       return search_bar
    }()
    
    let tab_gesture_recognizer : UITapGestureRecognizer = {
        let gesture_recognizer = UITapGestureRecognizer()
        gesture_recognizer.numberOfTapsRequired = 1
        gesture_recognizer.isEnabled = true
        gesture_recognizer.cancelsTouchesInView = false
        return gesture_recognizer
    }()
    
    let test_button : UIButton = {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 300, height: 100))
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .blue
        button.setTitle("Boton", for: .normal)
        button.layer.cornerRadius = 12.0
        return button
    }()
    
    let repositories_view_controller : RepositoriesViewController = {
       let view_controller = RepositoriesViewController()
        return view_controller
    }()
    
    let table_view : UITableView = {
        let table_view = UITableView()
        table_view.translatesAutoresizingMaskIntoConstraints = false
        return table_view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // agregando elementos
        view.addSubview(title_label)
        view.addSubview(search_bar)
        search_bar.delegate = self
        tab_gesture_recognizer.addTarget(self, action: #selector(singleTabHide))
        view.addGestureRecognizer(tab_gesture_recognizer)
        
        view.addSubview(table_view)
        table_view.delegate = self
        table_view.dataSource = self
        
        
        // estableciendo alineamientos
        setLayouts()
        
        
        // boton de prueba, se borrara al final
        //buttonConfig()
        
        view.backgroundColor = UIColor.white
    }
    
    // funcion para ocultar el teclado de busqueda
    @objc func singleTabHide(sender: UITapGestureRecognizer) {
        self.search_bar.endEditing(true)
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        search_bar.endEditing(true)
    }
    
    ///  aqui se realizara la composición de la vista
    private func setLayouts(){
        title_label.topAnchor.constraint(equalTo: view.topAnchor, constant: 100).isActive = true
        title_label.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
            
        search_bar.topAnchor.constraint(equalTo: title_label.bottomAnchor, constant: 15).isActive = true
        search_bar.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        
        table_view.topAnchor.constraint(equalTo: search_bar.bottomAnchor, constant: 10).isActive = true
        table_view.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        table_view.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -10).isActive = true
        
    }
    
    private func buttonConfig(){
        view.addSubview(test_button)
        test_button.addTarget(self, action: #selector(pushRepoView), for: .touchUpInside)        
        test_button.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -50).isActive = true
        test_button.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        test_button.widthAnchor.constraint(equalToConstant: 220) .isActive = true
    }
    
    @objc func pushRepoView(){
        self.navigationController?.pushViewController(repositories_view_controller, animated: true)
    }
    
    
    
    let apollo_client : ApolloClient = {
        
        let my_token = git_hub_token

        let apollo_client: ApolloClient = {
            let configuration = URLSessionConfiguration.default
            var authValue: String? = "Bearer \(my_token)"
            let authPayloads = ["Authorization": authValue ?? ""]
            let graphEndpoint = "http://api.github.com/graphql"
            configuration.httpAdditionalHeaders = authPayloads
            let endpointURL = URL(string: graphEndpoint)!
            return ApolloClient(
                networkTransport: HTTPNetworkTransport( url: endpointURL,  session: URLSession(configuration: configuration))
            )
        }()
        
        return apollo_client
    }()
    
    var users = [String?]()
    var emails = [String?]()
    
    func loadQuery(user_name : String!){
        let get_user_query = GetUserQlQuery(login: user_name)
        apollo_client.fetch(query: get_user_query) { results in
            do {
                // dado que la query solo retornaba un valor se adapto a fin de poder cerrar la demo
                guard let data = try? results.get().data else { return }
                self.users = [data.user?.name]
                self.emails = [data.user?.email]
            } catch let errorException {
                print("algo fallo")
            }
        }
    }
}

extension ViewController: UISearchBarDelegate {
    // Función de controlador de la barra de busqueda
    func searchBar(_ searchBar: UISearchBar, textDidChange textSearched: String) {
        let search_bar_text = search_bar.text ?? ""
        
        if search_bar_text.count > 1  {
            title_label.text = "Buscando ..." + search_bar_text
            self.loadQuery(user_name: search_bar_text)
            self.table_view.reloadData()
        }else{
            title_label.text = "Github Repositories"
            self.users = []
            self.table_view.reloadData()
        }
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.users.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UITableViewCell = {
            let cell = UITableViewCell()
            ///cell.translatesAutoresizingMaskIntoConstraints = false
            return cell
        }()
        let name_label : UILabel = {
            let label = UILabel()
            label.text = ""
            label.translatesAutoresizingMaskIntoConstraints = false
            return label
        }()
        
        let repo_description_label : UILabel = {
            let label = UILabel()
            label.text = "descripción"
            label.translatesAutoresizingMaskIntoConstraints = false
            return label
        }()
        
        let extra_text : UILabel = {
            let label = UILabel()
            label.text = "PR Count"
            label.translatesAutoresizingMaskIntoConstraints = false
            return label
        }()
        
                   
        name_label.text = self.users[indexPath.row]
        repo_description_label.text = self.emails[indexPath.row]
        
        cell.addSubview(name_label)
        cell.addSubview(repo_description_label)
        cell.addSubview(extra_text)
        
        name_label.topAnchor.constraint(equalTo: cell.topAnchor, constant: 16).isActive = true
        name_label.heightAnchor.constraint(equalToConstant: 20).isActive = true
        name_label.leftAnchor.constraint(equalTo: cell.leftAnchor, constant: 16).isActive = true
        
        repo_description_label.topAnchor.constraint(equalTo: name_label.bottomAnchor, constant: 16).isActive = true
        repo_description_label.heightAnchor.constraint(equalToConstant: 12.8).isActive = true
        repo_description_label.leftAnchor.constraint(equalTo: name_label.leftAnchor).isActive = true
        repo_description_label.widthAnchor.constraint(equalToConstant: 110).isActive = true
        
        extra_text.leftAnchor.constraint(equalTo: repo_description_label.rightAnchor, constant:  10).isActive = true
        extra_text.rightAnchor.constraint(equalTo: cell.rightAnchor, constant:  -10).isActive = true
        extra_text.topAnchor.constraint(equalTo: cell.topAnchor, constant: 20).isActive = true
        extra_text.bottomAnchor.constraint(equalTo: cell.bottomAnchor, constant: -20).isActive = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.pushRepoView()
        //self.loadQuery()
    }
    
    
}

